# parser => file_reader
# validator => model
# presenter => print
require_relative './model/validator'

gemfile_content = File.read ARGV[0]

validation_result = Validator.new.process gemfile_content
puts validation_result
