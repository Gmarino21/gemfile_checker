class Validator
  def process(gemfile_content)
    return 'Error: Gemfile vacio' if gemfile_content.empty?

    return 'Error: Gemfile sin source' unless gemfile_content.include?('source ')

    ruby = ruby_verification(gemfile_content)

    return 'Error: Gemfile sin ruby' unless ruby

    return 'Error: Gemfile gemas desordenadas' unless gem_verification(gemfile_content, ruby)

    'Gemfile correcto'
  end

  def ruby_verification(gemfile_content)
    ruby = false
    gemfile_content.each_line do |line|
      ruby ||= line.match(/ruby '[0-9].[0-9].[0-9]'/)
    end
    ruby
  end

  def gem_verification(content, ruby)
    return false unless ruby

    previous_line = ''
    content.each_line do |line|
      if line.include?('gem ')
        return false if previous_line > line
      end
      previous_line = line
    end

    true
  end
end
