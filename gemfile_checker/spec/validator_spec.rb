require 'rspec'
require_relative '../model/validator'

describe Validator do
  subject { @validator = described_class.new }

  it { is_expected.to respond_to :process }

  it 'should validate gemfile string and return string' do
    gemfile_string = ''
    result = subject.process gemfile_string
    expect(result).to eq 'Error: Gemfile vacio'
  end

  it 'should return true when valid Gemfile' do
    gemfile_content = File.read "#{__dir__}/samples/Gemfile.valid"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq 'Gemfile correcto'
  end

  it 'should return false when Gemfile dont have source specification' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.sin.source"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq 'Error: Gemfile sin source'
  end

  it 'should return false when Gemfile dont have ruby specification' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq 'Error: Gemfile sin ruby'
  end

  it 'should return false when Gemfile is empty' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.vacio"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq 'Error: Gemfile vacio'
  end

  it 'should return false when the file has messy gems' do
    gemfile_content = File.read  "#{__dir__}/samples/Gemfile.invalid.gem"
    validation_result = described_class.new.process gemfile_content
    expect(validation_result).to eq 'Error: Gemfile gemas desordenadas'
  end
end
